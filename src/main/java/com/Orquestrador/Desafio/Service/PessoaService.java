package com.Orquestrador.Desafio.Service;

import com.Orquestrador.Desafio.Entity.Pessoa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class PessoaService {

    @Autowired
    private RestTemplate restTemplate;

    public ResponseEntity<List> buscar(long cpf) {
        List<Pessoa> resultado = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List> response = restTemplate.getForEntity("http://localhost:8081/pessoa/buscar/"+cpf, List.class);
        return response;
    }

    public void insere(Pessoa pessoa) {
        restTemplate.postForObject("http://localhost:8081/pessoa/", pessoa, Object.class);
    }
}



