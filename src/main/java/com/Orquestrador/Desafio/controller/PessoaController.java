package com.Orquestrador.Desafio.controller;

import com.Orquestrador.Desafio.Entity.Pessoa;
import com.Orquestrador.Desafio.Service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @GetMapping("/consultar/{cpfPessoa}")
    public ResponseEntity<List> ConsultarPesssoa(@PathVariable long cpfPessoa){
        return pessoaService.buscar(cpfPessoa);
    }


    @PostMapping("/cadastrar")
    public Boolean CadastrarPessoa(Pessoa pessoa){
        try{
            pessoaService.insere(pessoa);
        }catch(Exception e){
            e.printStackTrace();
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
